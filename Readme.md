## Hedge Fund project

### Listing des pages :
- Home
- Team
- Dashboard
- History
- Buy and Sell shares
- Manage informatioon

**show/dashboard**: _/$user_id_

**edit**: _/$user_id/edit_

**delete**: _/$user_id/delete_

**index**: _/_

**history**: _/$user_id/history_

**share_profile**: _/$user_id/share_

**stats**: _/$user_id/stats_

**display_investments**: _/$user_id/invests_

**sell/buy**: _/$user_id/invest_

**convert**: _/$user_id/invests/convert_
