import Vue from 'vue'
import Vuex from 'vuex'
import App from './app'
import router from '@router'
import store from '@state/store'
import VueLocalStorage from 'vue-localstorage'
// Vuetify
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import axios from 'axios'
import VueAxios from 'vue-axios'

import FlashMessage from '@smartweb/vue-flash-message';
Vue.use(FlashMessage);

axios.defaults.baseURL = process.env.VUE_APP_API_BASE_URL

Vue.use(VueAxios, axios)

Vue.use(Vuetify);
Vue.use(Vuex);
Vue.use(VueLocalStorage);

const accessToken = localStorage.getItem('access_token')


if (accessToken) {
    Vue.prototype.$http.defaults.headers.common['Authorization'] =  accessToken
}

// Don't warn about using the dev version of Vue in development.
Vue.config.productionTip = process.env.NODE_ENV === 'production'

// If running inside Cypress...
if (process.env.VUE_APP_TEST === 'e2e') {
  // Ensure tests fail when Vue emits an error.
  Vue.config.errorHandler = window.Cypress.cy.onUncaughtException
}

const app = new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');


// If running e2e tests...
if (process.env.VUE_APP_TEST === 'e2e') {
  // Attach the app to the window, which can be useful
  // for manually setting state in Cypress commands
  // such as `cy.logIn()`.
  window.__app__ = app
}
