import News from './News'

describe('@views/News', () => {
  it('is a valid view', () => {
    expect(News).toBeAViewComponent()
  })
})
