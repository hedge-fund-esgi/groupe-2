FROM node:12.2.0-alpine

# install simple http server for serving static content
RUN npm install -g http-server

RUN apk add --no-cache \
    git \
    autoconf \
    automake \
    bash \
    g++ \
    libc6-compat \
    libjpeg-turbo-dev \
    libpng-dev \
    make \
    nasm

# make the 'front' folder the current working directory
WORKDIR /front

# copy both 'package.json' and 'package-lock.json' (if available)
COPY package.json /front

# install project dependencies
RUN npm install

# copy project files and folders to the current working directory (i.e. 'front' folder)
COPY . /front

# build front for production with minification
RUN npm run build

EXPOSE 8080
CMD [ "http-server", "dist" ]
