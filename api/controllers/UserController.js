const User = require('../models/userModel');
const jwt = require('jsonwebtoken');
const config = require('../config/secrets');

exports.index = function(req, res){
  User.find({}, function(err, users) {
    var userMap = {};

    users.forEach(function(user) {
      userMap[user._id] = user;
    });

    res.json(userMap);
  });
}

exports.getUser = function(req, res, next){
    decoded = jwt.verify(req.headers.authtoken, config.secrets.jwt_key);
    User.findOne({email:decoded.user.email},function(err, user){
      if(err) res.send(err);
      return res.status(200).json(user);
    });
};

exports.editUser = function(req, res, next){
  decoded = jwt.verify(req.headers.authtoken, config.secrets.jwt_key);
  User.findOneAndUpdate({email: decoded.user.email}, req.body, {new: true}, (err,done)=>{
    if(err) return res.status(500).send(err);
    return res.send(done);
  })
};

exports.removeUser = function(req, res, next){
    User.deleteOne({_id:req.params.userId},function(err,user){
        if(err) res.send(err);
        return res.status(200).json({status: "User deleted"});
    })
};
