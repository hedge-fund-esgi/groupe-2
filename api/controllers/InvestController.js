const User = require('../models/userModel');

exports.invests = function(req, res) {
    res.send('NOT IMPLEMENTED: User investements');
};

exports.sell = function(req, res) {
    res.send('NOT IMPLEMENTED: Sell user assets');
};

exports.buy = function(req, res) {
  var asset = req.body;
  if (asset.asset_name === null) {
    return res.json({error: "Asset name must be defined"})
  }else if (asset.asset_amount === null) {
    return res.json({error: "Asset amount must be defined"})
  }else {
    User.findOneAndUpdate({email: req.params.email}, req.body, {new: true}, (err,done)=>{
      if(err) return res.status(500).send(err);
      console.log(req.body);
      return res.send(done);
    })
  }
};

exports.convert = function(req, res) {
    res.send('NOT IMPLEMENTED: Convert user assets');
};
