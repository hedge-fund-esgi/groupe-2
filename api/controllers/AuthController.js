var User = require('../models/userModel');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const config = require('../config/secrets');

exports.register = function(req, res){
    var new_user = new User(req.body);
    console.log(req.body);
    if (new_user.password != req.body.password_confirmation) {
      return res.json({
        success: false,
        message: 'Password confirmation does not match with the password'
      });
    }else {
      var user_exists = User.findOne({email: req.body.email}, function(err, user){
        if(err) res.send(err);
        if (user === null) {
          bcrypt.hash(new_user.password, 10, function(err, hash){
            new_user.password = hash;
            new_user.save(function(err, user){
              if(err) res.send(err);
              res.json(user);
            });
          });
        }else {
          res.json({error: 'User with given email already exists'});
        }
      })
    }
};

exports.login = function(req, res){
  User.findOne({email: req.body.email}, function(err, user){
    if(err) res.send(err);
    if(user.email === req.body.email){
      bcrypt.compare(req.body.password, user.password, function(err, response) {
        if(response) {
          jwt.sign({user}, config.secrets.jwt_key, {expiresIn: '30 days'}, (err, token) => {
            if(err){
              return res.json({
                success: false,
                message: 'Something went wrong'
              });
            }else {
              res.setHeader('auth_token', token);
              res.status(200)
              return res.json({
                success: true,
                message: user
              });
            }
          })
        } else {
          console.log(res);
          return res.json({
            success: false,
            message: 'Invalid password'
          });
        }
      });

    }else{
      res.sendStatus(400);
    }
  })
}
