'use strict';

const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');


app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());


mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);


mongoose.Promise = global.Promise;


var mongodb = 'mongodb://db_mongo:27017/hedge_fund';
mongoose.connect(mongodb, {useNewUrlParser: true});
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));


// dotenv
if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config();
}

require('./routes/routes')(app);

// Constants
const PORT = process.env.API_PORT;
const HOST = process.env.API_HOST;

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
