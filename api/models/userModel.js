const mongoose = require('mongoose');

var Schema =  mongoose.Schema;
var assetsSchema = new Schema({
  name: {
    type: String
  },
  amount: {
    type: Number
  }
})
var userSchema = new Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    firstname:{
        type: String,
        required: true
    },
    lastname:{
        type: String,
        required: true
    },
    shared_profile:{
      type: Boolean
    },
    assets: assetsSchema

});
module.exports = mongoose.model('Users', userSchema);
