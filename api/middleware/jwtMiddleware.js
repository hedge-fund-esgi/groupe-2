const jwt = require('jsonwebtoken');
const config = require('../config/secrets');

exports.verify_token = function(req, res, next){
    var auth = req.headers['authtoken'];
    console.log(req.headers);
    if(typeof auth !== 'undefined'){
        jwt.verify(auth, config.secrets.jwt_key, (err, authData) => {
            if(err) res.sendStatus(403);
            next();
        });
    }else{
        res.sendStatus(403);
    }
};
