module.exports =  function(app){
    const cors = require("cors");

    var corsOptions = {
      origin: "*",
      exposedHeaders: "auth_token",
      methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
      preflightContinue: false,
      optionsSuccessStatus: 204
    }
    app.use(cors(corsOptions))

    var user_controller = require('../controllers/UserController');
    var management_controller = require('../controllers/ManagementController');
    var invest_controller = require('../controllers/InvestController');
    var auth_controller = require('../controllers/AuthController');
    var jwtMiddleware = require('../middleware/jwtMiddleware');

    app.get('/', function(req, res){
        res.send({hello: 'world'});
    });

    // AUTHENTIFICATION ROUTES
    app.post('/register', auth_controller.register);
    app.post('/login', auth_controller.login);
    app.get('/verify_token', jwtMiddleware.verify_token);

    // USER ROUTES
    app.get('/users', user_controller.index)
    app.get('/user/', user_controller.getUser);
    app.patch('/user/edit', jwtMiddleware.verify_token, user_controller.editUser)
    app.delete('/user/remove', jwtMiddleware.verify_token, user_controller.removeUser)
    app.get('/user/history', jwtMiddleware.verify_token, management_controller.history)
    app.get('/user/share_profile', jwtMiddleware.verify_token, management_controller.share_profile)
    app.get('/user/stats', jwtMiddleware.verify_token, management_controller.stats)
    app.get('/user/invests', jwtMiddleware.verify_token, invest_controller.invests)
    app.post('/user/sell', jwtMiddleware.verify_token, invest_controller.sell)
    app.post('/user/buy', jwtMiddleware.verify_token, invest_controller.buy)
    app.post('/user/convert', jwtMiddleware.verify_token, invest_controller.convert)
}
